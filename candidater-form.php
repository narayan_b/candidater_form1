<?php
/*
Plugin Name: Candidater Form Plugin
Description: A plugin for the candidater form.
Version: 1.0
Author: Narayan Bhattarai
*/


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get form data
    $first_name = sanitize_text_field($_POST['first_name']);
    $last_name = sanitize_text_field($_POST['last_name']);
    $date_of_birth = sanitize_text_field($_POST['date_of_birth']);
    $current_diploma = sanitize_text_field($_POST['current_diploma']);
    $current_study_level = sanitize_text_field($_POST['current_study_level']);
    $desired_formation = sanitize_text_field($_POST['desired_formation']);
    $city = sanitize_text_field($_POST['city']);
    $mobility = sanitize_text_field($_POST['mobility']);
    $free_text = sanitize_text_field($_POST['free_text']);
    
  
    
    // Insert data into the database
    global $wpdb;
    $table_name = $wpdb->prefix . 'candidater_data'; 
    
    $data = array(
        'first_name' => $first_name,
        'last_name' => $last_name,
        'date_of_birth' => $date_of_birth,
        'current_diploma' => $current_diploma,
        'current_study_level' => $current_study_level,
        'desired_formation' => $desired_formation,
        'city' => $city,
        'mobility' => $mobility,
        'free_text' => $free_text,        
    );
    
    $wpdb->insert($table_name, $data);
 }
 



function candidater_form_shortcode() {
    ob_start(); 
    ?>

    <form method="post" action="">
        <label for="first_name">First name:</label>
        <input type="text" name="first_name" id="first_name" required>

        <label for="last_name">Last name:</label>
        <input type="text" name="last_name" id="last_name" required>

        <label for="date_of_birth">Date of birth:</label>
        <input type="date" name="date_of_birth" id="date_of_birth" required>

        <label for="current_diploma">Current Diploma:</label>
        <input type="text" name="current_diploma" id="current_diploma" required>

        <label for="current_study_level">Current Study Level:</label>
        <input type="text" name="current_study_level" id="current_study_level" required>

        <label for="desired_formation">Desired Formation:</label>
        <input type="text" name="desired_formation" id="desired_formation" required>

        <label for="city">City of Habitation:</label>
        <input type="text" name="city" id="city" required>

        <label for="mobility">Mobility (in kilometers from the place of residence):</label>
        <input type="text" name="mobility" id="mobility" required>

        <label for="free_text">Free Text:</label>
        <textarea name="free_text" id="free_text"></textarea>

        <input type="submit" value="Submit">
    </form>


    <?php
    return ob_get_clean(); // Return the form content
 }
 
 add_shortcode('candidater_form', 'candidater_form_shortcode');
 
 function enqueue_candidater_form_styles() {
    wp_enqueue_style('candidater-form-styles', plugin_dir_url(__FILE__) . 'candidater-form.css');
}
add_action('wp_enqueue_scripts', 'enqueue_candidater_form_styles');
 